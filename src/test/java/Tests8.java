import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 20.04.18
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
public class Tests8 {
    WebDriver driver = initChromeDriver();
    @Test
    public void BaseTest(){
        driver.get("http://test1.cmstest.t.ukrtech.info/backend" );
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement field = driver.findElement(By.id("LoginForm_email"));
        field.sendKeys("admin");
        WebElement field1 = driver.findElement(By.id("LoginForm_password"));
        field1.sendKeys("12345678");
        WebElement entrance = driver.findElement(By.id("yw0"));
        entrance.click();
        WebElement kategori = driver.findElement(By.linkText("Категории"));
        kategori.click();
        WebElement dobav = driver.findElement(By.linkText("Добавить"));
        dobav.click();
        WebElement name = driver.findElement(By.id("Category_name")) ;
        name.sendKeys("Абрикос");
        WebElement alias = driver.findElement(By.id("Category_slug"));
        alias.sendKeys("abricos");
        WebElement button = driver.findElement(By.id("yw0"));
        button.click();
        WebElement main = driver.findElement
                (By.cssSelector("#overall-wrap > nav > div > div.navbar-header > a > img"));
        main.click();
        WebElement kategorii1 = driver.findElement(By.linkText("Категории"));
        kategorii1.click();
        WebElement newKategore = driver.findElement(By.xpath("//*[@id=\"category-grid\"]/table/tbody/tr[9]/td[3]/a"));
        String newKategoreText = newKategore.getText();
        assertEquals("Абрикос",newKategoreText);
        WebElement Eye = driver.findElement(By.xpath("//*[@id=\"category-grid\"]/table/tbody/tr[9]/td[9]/div/a[1]"));
        Eye.click();
        WebElement abr = driver.findElement(By.xpath("//*[@id=\"yw0\"]/tbody/tr[3]/td"));
        String abrText = abr.getText();
        assertEquals("Абрикос", abrText);
        driver.quit();


    }

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }


}
